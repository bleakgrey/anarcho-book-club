#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Herag Anarchivist'
SITENAME = 'Anarcho Book Club'
SITEURL = 'https://anarchobook.club'
PATH = 'content'
TIMEZONE = 'Etc/GMT-5'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
RSS_FEED_SUMMARY_ONLY = False
DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

AUTHORS_SAVE_AS = False
TAGS_SAVE_AS = False
INDEX_SAVE_AS = 'index.html'

# Ligo-specific vars
THEME = '/home/herag/anarchobookclub/themes/ligo'

LIGO_NAV_LINKS = ( 
    ('Library', SITEURL + '/pages/library.html'),
    ('About', SITEURL + '/pages/about.html'),
    ('RSS', SITEURL + '/ogg.xml'),
)

LIGO_FOOTER_TEXT = "We don't track you, but others do."
LIGO_FOOTER_LINKS = (
    ('Mastodon', 'https://dobbs.town/@herag'),
    ('Lemmy', 'https://lemmy.ml/c/anarchobookclub'),
    ('Email', 'mailto:anarchobookclub@riseup.net'),
    ('Copyleft', 'http://www.wtfpl.net/txt/copying/')
)
