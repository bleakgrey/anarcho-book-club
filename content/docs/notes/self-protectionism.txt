On Self-Protectionism

Petr Kropotkin's claim that Communism springs from the
development of individualism in the past three-hundred years is most
interesting. The idea that the individual begins to become aware of his/her
own individual power enough that they want to protect themselves and their
families and friends from abuse by those that would seek to loot and harm
them is very evident throughout history. Even in Medieval Scandinavian
society, there was a strong impulse for self-protectionism.

Before the onset of Christianity in the northern lands, there was a strong
impulse to individualism and the gain of personal wealth. Eventually, with
the migration to Iceland, there also developed a strong impulse for
communalism, as evidenced in the Icelandic Sagas and the writing in the
Poetic/Elder Edda.