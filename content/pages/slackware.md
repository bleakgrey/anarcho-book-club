Title: Slackware Linux
Subtitle: Get Slack
Date: 03-30-2021
save_as: pages/slackware.html

![Slackware Linux Logo]({static}/images/slackware.jpg)

## Why Slackware?

I have been a long-time Slacker. Slackware is the all-in-one operating system created by Patrick Volkerding. It is unlike any other OS out there because it literally has 9 gigabytes of software contained in the installation disks of the system. It is also one of the most stable operating systems out there because the Pat is a very particular gatekeeper. As a result, the software is not as up-to-date as some other operating systems.

## Installation

In the most classy way ever, you will find Slack at <http://slackware.com> under the link that says [Get Slack](http://slackware.com/getslack). As the great J.R. "Bob" Dobbs himself once said, "You gotta get that slack in your life!" Go ahead and click through to get yourself some slack.

One of the longtime favorite pieces of software for writing ISOs to USB drives is called [Unetbootin](https://unetbootin.org). Once you get this installed, it is pretty straightforward how to write the ISO you downloaded and verified to the USB stick.

Once it's written, it's time to reboot. Now depending on the make and model of your computer there may be a different key during reboot to get to the BIOS. We need to get to the BIOS so that our computer will allow us to change the Operating System that is loaded at boot time. This is also pretty simple. Usually it is the `<F12>` or `<Delete>` key that gets you to the BIOS menu, but some computers do things a little differently. So consult DuckDuckGo again for your specific computer if `<F12>` or `<Delete>` does not work.

Once in the BIOS, it's time to select the USB drive to boot from. It's usually in a section labelled `<BOOT>` or something to that effect. You should find the name of your USB drive there. Mine is often a PNY Flash or something of that nature. You'll want to boot from it and wait for Slackware to load.

The first screen you come to will be a TTY interface. Just read the text carefully and you'll know what you need to do. If you don't know, DuckDuckGo is your best friend. I don't plan on going much further into depth on this, as there are a lot of better guides out there, including the official channel at [Slackware: Installation Help](http://slackware.com/install).

#### Note: Hopefully you have already backed up any important files. Don't worry if they are Word or Excel documents. We'll be able to access them with our fancy, new Free Software!


