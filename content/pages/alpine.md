Title: Alpine Linux
Subtitle: Small. Simple. Secure.
Date: 03-30-2021
save_as: pages/alpine.html

![Alpine Linux Logo](images/alpine.svg)

## Why I Like This Operating System

Let's start by talking about why this OS is at the top of this list. 

Alpine Linux is simple. It is small. It doesn't assume its user. Alpine Linux also tries to keep the underlying code of the OS clean and very specific. I won't get into the nitty gritty here. There are scores of websites that will expand on these things. Suffice it to say that Alpine Linux is best used on minimal systems as well as huge server systems. It allows the user to start with a bare, minimum system and build it out from there.

## Installation

*This installation guide is taken directly from Alpine Linux's own page. If it has been modified at all, it is to make it a little more generalized. You can find their installation guide at the [Alpine Wiki](wiki.alpinelinux.org/wiki/installation). This guide also assumes that you are coming from the Windows Operating System and should be version neutral.*

*Another thing to note is that Alpine Linux tends to be a very commandline driven operating system. If this makes you uncomfortable or you'd rather start with something a little easier go ahead and skip this one and try [Slackware](pages/slackware.html) for medium difficulty and [elementaryOS](pages/elementary.html) for super easy difficulty.*

### Getting Alpine

Head over to <https://alpinelinux.org/downloads/> and grab the Alpine software that best fits the type of processor that you have. The majority of basic users will want the x86_64 image for a regular laptop or desktop computer. If you have questions about what type of computer you have, [DuckDuckGo](https://duckduckgo.com) is your friend. Once you figure it out, download the image and we will proceed.

*It is advised at this point to verify the download to make sure the file has not been infiltrated by bad JavaScript or some other nefarious code. To do that I have found a pretty useful guide [here](https://www.shellhacks.com/windows-md5-sha256-checksum-built-in-utility) to help you do that.*

Once verified, we will need to write the ISO file to a USB stick.

One of the longtime favorite pieces of software for writing ISOs to USB drives is called [Unetbootin](https://unetbootin.org). Once you get this installed, it is pretty straightforward how to write the ISO you downloaded and verified to the USB stick.

Once it's written, it's time to reboot. Now depending on the make and model of your computer there may be a different key during reboot to get to the BIOS. We need to get to the BIOS so that our computer will allow us to change the Operating System that is loaded at boot time. This is also pretty simple. Usually it is the `<F12>` or `<Delete>` key that gets you to the BIOS menu, but some computers do things a little differently. So consult DuckDuckGo again for your specific computer if `<F12>` or `<Delete>` does not work.

After you've gotten to the BIOS, you can simply boot from the Alpine USB Drive that we just created with Unetbootin and after it loads up, you should be at the TTY for Alpine. That's it! You have booted up to an OS other than Windows! Now let's get that wrtten to the harddrive and purge Windows from existence.

#### Note: Hopefully you have already backed up any important files. Don't worry if they are Word or Excel documents. We'll be able to access them with our fancy, new Free Software!

### setup-alpine

Alpine Linux comes with a bunch of nice little scripts to help you setup your environments and to help you get things running fairly quickly. The rest of this guide will cover setting up a traditional installation, though Alpine Linux can also be installed to simply run in RAM. 

The first thing you will do after boot is type `<root>` as the username. That will give you unfettered access to everything on your computer and allow you to install Alpine as your OS.

After you his `<RETURN>`, you will be dropped into a regular TTY session and the instructions are very simple from here on out.

Type:

`<setup-alpine>`

Then the installer will guide you through everything. The cool thing about this is that you can mess around with everything. I use UTC for my timezone because it is standard and helps coordinate different projects that I have with different people around the world.

*At this point there is no point in re-inventing the wheel, so I will point you to the [Alpine Installation](https://wiki.alpinelinux.org/wiki/installation/) wiki to finish up the basic installation. From this point on, I assume you have it installed as `<sys>` so that it is a traditional installation. Also, go ahead an read through to the end of that wiki article and add the different pieces of software that you might want/need for your specific setup. It should be straightforward, really.*

### Software

We will cover installing the [Mate Desktop Environment](https://mate-desktop.org) here, as it is probably the cleanest and simplest setup you can get. There are other options as well. The [XFCE4 Desktop Environment](https://xfce.org) is also officially supported, as well as many others in the Community Repository. For now we will focus on installing Mate.

First you'll want to make sure you have the community repo enabled. To do this you will need to edit the file `</ect/apk/repositories>`. We do this using a program called `<vi>`. That is the text editor that comes by default with Alpine Linux. To do this, simply type `<vi /etc/apk/repositories>` and then hit `<RETURN>`.

This will bring up a page with a few links. You'll see the *#* 


















