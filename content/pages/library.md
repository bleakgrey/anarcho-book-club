Title: Library
Subtitle: A classic is something that everybody wants to have read and nobody wants to read. -Mark Twain
Date: 2021-03-23 15:12
save_as: pages/library.html
header_cover: images/library.png



## Bakunin, Michael

[Federalism, Socialism, Antitheologism]({static}/docs/bakunin/federalism-socialism-antitheologism.epub)

[God and the State]({static}/docs/bakunin/god-and-the-state.epub)

[Man, Society, and Freedom]({static}/docs/bakunin/man-society-and-freedom.epub)

[Marxism, Freedom, and the State]({static}/docs/bakunin/marxism-freedom-and-the-state.epub)

[Stateless Socialism Anarchism]({static}/docs/bakunin/stateless-socialism-anarchism.epub)

[The Capitalist System]({static}/docs/bakunin/the-capitalist-system.epub)

[The Class War]({static}/docs/bakunin/the-class-war.epub)

[The Commune, The Church, The State]({static}/docs/bakunin/the-commune-the-church-the-state.epub)

[The Immorality of the State]({static}/docs/bakunin/the-immorality-of-the-state.epub)

## Chomsky, Noam

[On Anarchism]({static}/docs/chomsky/on-anarchism.epub)

[Optimism Over Despair]({static}/docs/chomsky/optimism-over-despair.epub)

## Emma Goldman

[Anarchism and Other Essays]({static}/docs/goldman/anarchism.epub)

[The Individual, Society, and the State]({static}/docs/goldman/the-individual-society-and-the-state.epub)

## Graeber, David

[Bullshit Jobs]({static}/docs/graeber/bullshit-jobs.epub)

[Debt: The First 5000 Years]({static}/docs/graeber/debt.epub)

## Kafka, Franz

[The Metamorphosis]({static}/docs/kafka/the-metamorphosis.epub)

## Klaatu

You can get it from [SmashWords](https://www.smashwords.com/books/view/582453) or download it here [Computing Without Compromise]({static}/docs/klaatu/computing-without-compromise.epub)

## Konkin, Samuel Edward, III

[Agorist Class Theory]({static}/docs/konkin/agorist-class-theory.epub)

## Kropotkin, Petr

[The Conquest of Bread]({static}/docs/kropotkin/the-conquest-of-bread.epub)

[Mutual Aid]({static}/docs/kropotkin/mutual-aid.epub)

## Vonnegut, Kurt

[Player Piano]({static}/docs/vonnegut/player-piano.epub)

## Whitman, Walt

[Leaves of Grass]({static}/docs/whitman/leaves-of-grass.epub)


