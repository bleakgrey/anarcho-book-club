Title: About
Subtitle: Anarchy, Free Software, What Licensing, No Compromise
Date: 2021-03-05
save_as: pages/about.html
header_cover: images/books.jpg

## Anarchy

Anarchy is the term that this book club uses to describe that which is inherently human, that which is not ruled over or held down by an oppressive power. Anarchy is a state of being, not a way of life or a religion or a political party. Anarchy is choosing to say, *I am human, I am an individual*. There are many different creeds and manifestos proclaiming anarchy in the name of one some political movement, but the Anarcho Book Club recognized that anarchy starts in the mind of the individual and spreads outward to affect the lives of those around us, asserting that no power can assume dominance over us.

## Free Software

The goal of anarchism is pure and unadulterated freedom. Part of the issue of freedom is that so many claim that they are free, but voluntarily chain themselves to devices that are not free. Anarchists must realize that freedom means that we must be free to exist without oppression.

Oppression, in the technical sense, is the oppression of massive companies and overreaching governments. The only way to secure such freedom is through free and open source software. This site and its administrator urges any listeners/readers to strive to use only free and open source software in their daily lives. 

To that effect we will soon have pages up for the best free software out there that we can find as a separate part of the site.

## What Licensing

Anarcho Book Club *always* uses the [WTFPL](https://www.wtfpl.net). This is because, as anarchists, we do not believe in the legitimacy of licenses granted by any government or organization as a means of establishing a 'right' over what is commonly referred to as *Intellectual Property*. However, if a piece of data is not licensed permissively, the creators of that data can be eventually locked out. So to combat that, the Anarcho Book Club found the most permissive license out there and applied it all to every scrap of data we create.

## No Compromise

When we speak of anarchist principles, or principles of freedom, we are talking about the things in all of our lives that affect us. No Compromise is the pledge to live free of the rule of the elite, be it in our daily analog lives or in our digital lives. We always look for the path that gives us the most freedom, whether it be by choosing free software, when proprietary software is simply more convenient to our purpose, or choosing to look to an alternative to the mega corps for things like food and daily necessities. We strive every day to live free of compromise in order to become the anarchists that we claim to be.

## In Summary

Freedom is a choice that we all must make. To throw off the chains of the oppressor and fight back in our own way is the utmost of our cause. The armaments provided to us are knowledge and wisdom, which we, at the Anarcho Book Club, seek to mine and eek out of ever morsel of text that we can find. Pair that with the power of Free Software the strength of Human Action, we can accomplish any goal that we set our minds to. 
