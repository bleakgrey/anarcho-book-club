Title: CyberFreedom
Subtitle: Free Software for a Free World
Date: 2021-03-30
save_as: pages/cyberfreedom.html

*Part of seeking freedom from oppression is seeking freedom through the tech that we use. As we have discussed on the show before, there are a number of free software projects out there that can help a user retain their own freedom from the oppression by mega tech corporations. Hopefully this helps someone out there with their endeavors in this world.*

## Operating Systems

*An operating system is a set of software that aids the user in their computing tasks. Below are recommendations of Anarcho Book Club based on their simplicity and freedom respectability. You will notice that most of them use Linux as their base, but don't be confused; Linux is not the operating system, it is merely the kernel, the underlying code that allows the user to interface with the hardware. The Operating System is merely the set of software that allows the user to be productive. Some are easy to use, while others offer a little more of a challenge. If you have any questions, feel free to email the club or send us a message on Mastodon. The goal of these guides is not to cherry pick those that I like best, but to give you a look at the systems that you could be using as an alternative to the systems that continually spy on you and attempt to control you.*

![Alpine Linux logo]({static}/images/alpine.png)

[Alpine Linux](https://alpinelinux.org) is, by far, my very favorite for more advanced uses as well as for its simplicity. It's initial interface is a simple TTY terminal, but that can be remedied very easily for the user that prefers/requires a Graphical User Interface (GUI). I personally prefer the Mate interface for its clean simplicity and very traditional, straightforward design. In this we will walk through a basic guide on how to install Alpine Linux and a few basic programs that you may want.

---

![Slackware Linux logo]({static}/images/slackware.jpg)

If you are looking for an Operating System that has everything to offer, [Slackware](http://www.slackware.com) is for you. It gets its name from the great [Church of the SubGenius](http://subgenius.com). It boasts a 9 gigabyte install, which includes almost every piece of software a basic user could ever want. It is also, by far, one of the most stable Linux Operating Systems out there. On the stable release channel, the user should only expect security updates and some basic bigfixes to the existing software, but more than likely, with this OS, your system will not break from updates in any way. This is by far my very favorite production ready OS. In this guide, I'll show you have to setup a basic Slackware system and get you up and running in no time, freeing your computer of the buggy and bloated Windows OS.

---

![elementaryOS logo]({static}/images/elementary.png)

Easy street has never looked so good. With [elementaryOS](https://elementary.io) you have the easiest setup of all, including a number of features like a dedicated software manager calls AppCenter. It is extremely easy to use and best of all, you can help support the independent developers of the software you use as easily as clicking a button. This operating system is easy to use, but the trade off will be that you full control of your system is made a bit more difficult for the sake of beauty. This is not a bad thing. The beauty of this operating system far surpasses any proprietary OS on the market. What you gain is a freedom along with that beauty that is far more valuable than anything that a propriety and locked down system could ever offer. We will cover the install of this system along with a number of the apps that may be useful for you in this system.
