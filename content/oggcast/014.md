Title: Worldview
Date: 2021-03-05
Modified: 2021-03-11
Category: literature
slug: 014
authors: Herag Anarchivist
summary: In which we discuss the concept of worldview.

Today we skip out on our duties of going through *Song of Myself* and discuss the conept of worldview with a fun little story about my childhood and computing in the mix. In this, I try to outline a little more of how I see the concepts of anarchism in the greater world and as a greater concept/philosophy/mthology.

[GNU World Order](https://gnuworldorder.info)

[Leaves of Grass](/docs/whitman/leaves-of-grass.epub)

Into/Outro music is from Kai Engel's Chapter Four - Fall album. You can get that album and many more of his albums from his Bandcamp [page](https://kaiengel.bandcamp.com/).

<audio controls><source src="oggs/014-worldview.ogg" type="audio/ogg"></audio>

